//
//  MainCollectionViewCell.swift
//  InfiniteHorizontalScrolling
//
//  Created by Chanchal-PC on 14/06/16.
//  Copyright © 2016 Chanchal-PC. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!

}
