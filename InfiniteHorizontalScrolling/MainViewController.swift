//
//  MainViewController.swift
//  InfiniteHorizontalScrolling
//
//  Created by Chanchal-PC on 14/06/16.
//  Copyright © 2016 Chanchal-PC. All rights reserved.
//

import UIKit

class MainViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var FlowLayout: UICollectionViewFlowLayout!

    let reuseIdentifier = "MainCell"
    var datasource = []
    var indexPathforDeviceOrientation : NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datasource = [UIImage(named: "3")!,UIImage(named: "4")!,UIImage(named: "5")!,UIImage(named: "6")!,UIImage(named:"7")!]
        let firstItem = datasource.firstObject as? UIImage
        let lastItem = datasource.lastObject as? UIImage
        let workingarray = datasource.mutableCopy()
        workingarray.insertObject(lastItem!, atIndex: 0)
        workingarray.addObject(firstItem)
        datasource = workingarray as! NSArray
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var token: dispatch_once_t = 0
        dispatch_once(&token) { () -> Void in
            print("Called once")
           self.collectionView!.scrollToItemAtIndexPath(NSIndexPath(forItem: 1, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return datasource.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let   cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MainCollectionViewCell
        
      cell.imageView.image = datasource[indexPath.row] as? UIImage
        return cell
    }
    
   
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return self.view.bounds.size;
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
             indexPathforDeviceOrientation = self.collectionView?.indexPathsForVisibleItems().first
             self.collectionView?.collectionViewLayout.invalidateLayout()
    }


   
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        
        self.collectionView?.scrollToItemAtIndexPath(indexPathforDeviceOrientation!, atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
    }
//    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
//        
//       let contentOffsetWhenFullyScrolledRight = self.collectionView!.frame.size.width * CGFloat( datasource.count-1)
//
//        
//        if (scrollView.contentOffset.x == contentOffsetWhenFullyScrolledRight) {
//            
//           let newIndexPath = NSIndexPath.init(forRow: 1, inSection: 0)
//            
//        self.collectionView?.scrollToItemAtIndexPath(newIndexPath, atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)
//            
//         
//            
//        }
//        
//        
//        else if (scrollView.contentOffset.x == 0)  {
//            
//            // user is scrolling to the left from the first item to the fake 'item N'.
//            // reposition offset to show the 'real' item N at the right end end of the collection view
//            let newIndexPath = NSIndexPath.init(forRow: datasource.count-2, inSection: 0)
//            
//            self.collectionView?.scrollToItemAtIndexPath(newIndexPath, atScrollPosition: UICollectionViewScrollPosition.Left, animated: false)
//            
//           
//        }
//
//    
//    }
    
//    override func scrollViewDidScroll(scrollView: UIScrollView) {
//        
//        var lastcontentOffsetX :Float = 0
//        if(0 == lastcontentOffsetX)
//        {
//            
//            lastcontentOffsetX =  Float(scrollView.contentOffset.x)
//            return
//        }
//    
//  
//    
//   let currentOffsetX = Float(scrollView.contentOffset.x);
//   let currentOffsetY = Float(scrollView.contentOffset.y);
//    
//    let pageWidth = Float(scrollView.frame.size.width);
//    let  offset = pageWidth * Float((datasource.count - 2))
//    
//
//    if (currentOffsetX < pageWidth && lastcontentOffsetX > currentOffsetX) {
//    lastcontentOffsetX = currentOffsetX + offset;
//    scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
//    }
//    // the last page (showing the first item) is visible and the user's finger is still scrolling to the left
//    else if (currentOffsetX > offset && lastcontentOffsetX < currentOffsetX) {
//    lastcontentOffsetX = currentOffsetX - offset;
//    scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
//    } else {
//    lastcontentOffsetX = currentOffsetX;
//    }
//    
//    
//    
//    
//    }
    
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
  
        
        var lastcontentOffsetX :Float = 0
        if(0 == lastcontentOffsetX)
        {
            
            lastcontentOffsetX =  Float(scrollView.contentOffset.x)
            return
        }
        
        
        
        let currentOffsetX = Float(scrollView.contentOffset.x);
        let currentOffsetY = Float(scrollView.contentOffset.y);
        
        let pageWidth = Float(scrollView.frame.size.width);
        let  offset = pageWidth * Float((datasource.count - 2))
        
        
        if (currentOffsetX < pageWidth && lastcontentOffsetX > currentOffsetX) {
            lastcontentOffsetX = currentOffsetX + offset;
            scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
        }
            // the last page (showing the first item) is visible and the user's finger is still scrolling to the left
        else if (currentOffsetX > offset && lastcontentOffsetX < currentOffsetX) {
            lastcontentOffsetX = currentOffsetX - offset;
            scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
        } else {
            lastcontentOffsetX = currentOffsetX;
        }
        
        
        
        
    }
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        
        
        var lastcontentOffsetX = CGFloat(FLT_MIN)
        if(CGFloat(FLT_MIN) == lastcontentOffsetX)
        {
            
            lastcontentOffsetX =  scrollView.contentOffset.x
            return
        }
        
        
        
        let currentOffsetX = scrollView.contentOffset.x;
        let currentOffsetY = scrollView.contentOffset.y;
        
        let pageWidth = scrollView.frame.size.width
        let  offset = pageWidth * CGFloat((datasource.count - 2))
        
        
        if (currentOffsetX < pageWidth && lastcontentOffsetX > currentOffsetX) {
            lastcontentOffsetX = currentOffsetX + offset;
            scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
        }
            // the last page (showing the first item) is visible and the user's finger is still scrolling to the left
        else if (currentOffsetX > offset && lastcontentOffsetX < currentOffsetX) {
            lastcontentOffsetX = currentOffsetX - offset;
            scrollView.contentOffset = CGPoint(x: Int(lastcontentOffsetX), y: Int(currentOffsetY))
        } else {
            lastcontentOffsetX = currentOffsetX;
        }
        
        
        
        
    }
    
    
    
    


}
